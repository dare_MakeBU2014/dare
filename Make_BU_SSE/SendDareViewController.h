//
//  SendDareViewController.h
//  Make_BU_SSE
//
//  Created by Tom Strissel on 3/21/14.
//  Copyright (c) 2014 Make_BU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendDareViewController : UIViewController <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *writeDare;

@end
