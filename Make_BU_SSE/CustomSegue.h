//
//  CustomSegue.h
//  Make_BU_SSE
//
//  Created by Tom Strissel on 3/22/14.
//  Copyright (c) 2014 Make_BU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomSegue : UIStoryboardSegue

@end
