//
//  ContactViewController.h
//  Make_BU_SSE
//
//  Created by Tom Strissel on 3/22/14.
//  Copyright (c) 2014 Make_BU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
//#import "PeoplePickerViewControllerWrapper.h"

@interface ContactViewController : UIViewController <ABPeoplePickerNavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *myName;
@property (weak, nonatomic) IBOutlet UILabel *myNumber;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSMutableArray *contactArray;
@property (nonatomic) NSMutableArray *phoneNumbers;
- (IBAction)goButton:(id)sender;
@end
