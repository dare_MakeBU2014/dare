//
//  DaresViewController.m
//  Make_BU_SSE
//
//  Created by Tom Strissel on 3/22/14.
//  Copyright (c) 2014 Make_BU. All rights reserved.
//

#import "DaresViewController.h"
#import "CameraViewController.h"
@interface DaresViewController ()

@end

@implementation DaresViewController
@synthesize daresArray;
@synthesize tableView;
@synthesize numDares;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.daresArray = [NSMutableArray new];
    [daresArray addObject:@"Go to CityCo Naked"];
    [daresArray addObject:@"Build an iPhone app"];
    [daresArray addObject:@"Drink a gallon of milk"];
    [daresArray addObject:@"Scream like a girl outside"];
    [daresArray addObject:@"Eat a banana while dancing"];
    NSString *strFromInt = [NSString stringWithFormat:@"%d",daresArray.count];
    self.numDares.text = strFromInt;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%lu", (unsigned long)daresArray.count);
    return daresArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cellForRowAtIndexPath");
    static NSString * identifer = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifer];
    }
    NSLog(@"cellForRowAtIndexPath");
    
    cell.textLabel.text = self.daresArray[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *cellText = cell.textLabel.text;
    NSInteger *indexRow = indexPath.row;
    
    
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"chooseDare"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        CameraViewController *destViewController = segue.destinationViewController;
        destViewController.dareName = [daresArray objectAtIndex:indexPath.row];
}
}


@end
