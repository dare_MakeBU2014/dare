//
//  CameraViewController.h
//  Make_BU_SSE
//
//  Created by Tom Strissel on 3/21/14.
//  Copyright (c) 2014 Make_BU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface CameraViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (copy,   nonatomic) NSURL *movieURL;
@property (strong, nonatomic) MPMoviePlayerController *movieController;
@property (weak, nonatomic) IBOutlet UILabel *dareLabel;
@property (nonatomic, strong) NSString *dareName;

@end
