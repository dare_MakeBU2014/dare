//
//  InboxViewController.h
//  Make_BU_SSE
//
//  Created by Tom Strissel on 3/22/14.
//  Copyright (c) 2014 Make_BU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InboxViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
